package assignment2;

class Bottles {

	public String verse(int verseNumber) {
		String adding;
		adding= String.valueOf(verseNumber);
		if(verseNumber>1){ adding=adding + " bottles of beer on the wall, "; }
		else if (verseNumber==1){adding=adding + " bottle of beer on the wall, ";}
		else{adding="No more bottles of beer on the wall, ";}

		if (verseNumber>1){adding=adding+String.valueOf(verseNumber) + " bottles of beer.\n";}
		else if(verseNumber==1){adding=adding+String.valueOf(verseNumber) + " bottle of beer.\n";}
		else{adding=adding+"no more bottles of beer.\n";}

		if (verseNumber>1){adding=adding+"Take one down and pass it around, ";}
		else if (verseNumber==1){adding=adding+"Take it down and pass it around, ";}
		else{adding=adding+ "Go to the store and buy some more, " ;}

		if (verseNumber>2){adding=adding+String.valueOf(verseNumber-1)+" bottles of beer on the wall.\n";}
		else if (verseNumber>1){adding=adding+String.valueOf(verseNumber-1)+" bottle of beer on the wall.\n";}
		else if(verseNumber==1){adding=adding+"no more bottles of beer on the wall.\n";}
		else{adding=adding+"99 bottles of beer on the wall.\n";}


		return adding;
	}

	public String verse(int startVerseNumber, int endVerseNumber) {
		String adding="";

		for(int i=startVerseNumber; i>=endVerseNumber; i--){
			adding=adding+ verse(i);
			if(i!=endVerseNumber){adding=adding+"\n";}
		}

		return adding;
	}

	public String song() {
		String adding="";

		for(int i=99; i>=0; i--){
			adding=adding+ verse(i);
			if(i!=0){adding=adding+"\n";}
		}

		return adding;
	}
}
